<?php
include_once "controller/objectionController.php";

class ObjectionTest extends \PHPUnit\Framework\TestCase
{

    protected $objectionController;

    protected function setUp() :void {
        $this->objectionController = new  objectionController();

        //assuming that database contains this student with enough balance
        $data=json_decode('{"matNo":4492, "firstName":"test", "lastName":"test", "year": 4, "courseTitle": "test", "oldMark":56, "teacher":"test"}');
        $this->objectionController->submitObjection($data);
    }

    protected function tearDown() :void {
        $data=json_decode('{"matNo":4492, "courseTitle": "test"}');
        $this->objectionController->deleteObjection($data);
    }


    /******************************/

    /**
     * @test
     * @dataProvider getUserObjectionsDataEmptyProvider
     */
    public function testGetUserObjectionsDataEmpty($str) {
        $data = json_decode($str);
        $this->assertSame( $this->objectionController->getUserObjections($data), '{"message":"check your input data"}');
    }

    public function getUserObjectionsDataEmptyProvider():array {
        return [[''],
                ['{"matNo":"23dfg45"}'],
               ];
    }

    public function testGetUserObjectionsSuccess() {
        $data = $data=json_decode('{"matNo":4492}');
        $this->assertStringContainsString('"matNo":"4492","firstName":"test","lastName":"test","year":"4","courseTitle":"test","teacher":"test","oldMark":"56","status":"pending","newMark":null}', $this->objectionController->getUserObjections($data));
    }


    /************************************ */

    /**
     * @test
     * @dataProvider getObjectionsByStatusAndCourseTitleDataEmptyProvider
     */
    public function testGetObjectionsByStatusAndCourseTitleDataEmpty($str) {
        $data = json_decode($str);
        $this->assertSame( $this->objectionController->getObjectionsByStatusAndCourseTitle($data), '{"message":"check your input data"}');
    }

    public function getObjectionsByStatusAndCourseTitleDataEmptyProvider():array {
        return [[''],
                ['{"status":"pending"}'],
                ['{"courseTitle":"test"}'],
               ];
    }

    public function testGetObjectionsByStatusAndCourseTitleSuccess() {
        $data = $data=json_decode('{"status":"pending", "courseTitle":"test"}');
        $this->assertStringContainsString('"matNo":"4492","firstName":"test","lastName":"test","year":"4","courseTitle":"test"', $this->objectionController->getObjectionsByStatusAndCourseTitle($data));
    }


    /************************* */

    /**
     * @test
     * @dataProvider submitDataEmptyProvider
     */
    public function testSubmitDataEmpty($str) {
        $data = json_decode($str);
        $this->assertSame( $this->objectionController->submitObjection($data),'{"message":"Check your input data!"}');
    }


    public function submitDataEmptyProvider():array {
        return [[''],
                ['{"matNo":4565}'],
                ['{"matNo":4565, "firstName:"test"}'],
                ['{"matNo":4565, "firstName:"test", "lastName":"test"}'],
                ['{"matNo":4565, "firstName:"test", "lastName":"test", "year": 4}'],
                ['{"matNo":4565, "firstName:"test", "lastName":"test", "year": 4, "courseTitle": "test"}'],
                ['{"matNo":4565, "firstName:"test", "lastName":"test", "year": 4, "courseTitle": "test", "oldMark":56}'],
                ['{"matNo":-4565, "firstName:"test", "lastName":"test", "year": 4, "courseTitle": "test", "oldMark":56, "teacher":"test"}'],
                ['{"matNo":4565, "firstName:"test", "lastName":"test", "year": -4, "courseTitle": "test", "oldMark":56, "teacher":"test"}'],
                ['{"matNo":4565, "firstName:"test", "lastName":"test", "year": -4, "courseTitle": "test", "oldMark":-56, "teacher":"test"}'],
               ];
    }

    public function testSubmitSuccess() {
        $data=json_decode('{"matNo":4492, "firstName":"test2", "lastName":"test2", "year": 4, "courseTitle": "test2", "oldMark":56, "teacher":"test2"}');
        $output = $this->objectionController->submitObjection($data);

        $data=json_decode('{"matNo":4492, "courseTitle": "test2"}');
        $this->objectionController->deleteObjection($data);
        $this->assertSame($output,'{"message":"Objection request submitted Successfully."}');
    }

    public function testSubmitNotFound() {
        $data=json_decode('{"matNo":44924576657768, "firstName":"test2", "lastName":"test2", "year": 4, "courseTitle": "test2", "oldMark":56, "teacher":"test2"}');
        $this->assertSame($this->objectionController->submitObjection($data),'{"message":"No such User"}');
    }

    public function testSubmitConflict() {
        $data=json_decode('{"matNo":4492, "firstName":"test", "lastName":"test", "year": 4, "courseTitle": "test", "oldMark":56, "teacher":"test"}');
        $this->assertSame($this->objectionController->submitObjection($data),'{"message":"Objection request already exists"}');
    }


    /*********************************** */

    /**
     * @test
     * @dataProvider updateStatusDataEmptyProvider
     */
    public function testUpdateStatusDataEmpty($str) {
        $data = json_decode($str);
        $this->assertSame( $this->objectionController->updateStatus($data),'{"message":"check your input data"}');
    }


    public function updateStatusDataEmptyProvider():array {
        return [[''],
                ['{"id":24}'],
                ['{"newStatus":"pending"}'],
                ['{"id":"44" ,"newStatus":"pending"}'],
               ];
    }


    public function testUpdateStatusSuccess() {
        $data=json_decode('{"id":24, "newStatus":"reviewing"}');
        $this->assertSame($this->objectionController->updateStatus($data),'{"message":"Status updated successfully"}');
    }

    public function testUpdateStatusNotFound() {
        $data=json_decode('{"id":44924576657768, "newStatus":"reviewing"}');
        $this->assertSame($this->objectionController->updateStatus($data),'{"message":"No such objection."}');
    }


    /*************************** */


    /**
     * @test
     * @dataProvider enterResultDataEmptyProvider
     */
    public function testEnterResultDataEmpty($str) {
        $data = json_decode($str);
        $this->assertSame( $this->objectionController->enterResult($data),'{"message":"check your input data"}');
    }

    public function enterResultDataEmptyProvider():array {
        return [[''],
                ['{"id":24}'],
                ['{"newStatus":"pending"}'],
                ['{"id":"44" ,"newStatus":"pending"}'],
               ];
    }


    public function testEnterResultSuccess() {
        $data=json_decode('{"id":24, "newStatus":"unchanged"}');
        $this->assertSame($this->objectionController->enterResult($data),'{"message":"Result updated successfully"}');
    }

    public function testEnterResultNotFound() {
        $data=json_decode('{"id":44924576657768, "newStatus":"reviewing"}');
        $this->assertSame($this->objectionController->enterResult($data),'{"message":"No such objection."}');
    }

}