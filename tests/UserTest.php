<?php
include_once "controller/userController.php";

class UserTest extends \PHPUnit\Framework\TestCase
{
    protected $userController;

    protected function setUp() :void {
        $this->userController = new  userController();
        $data=json_decode('{"username":"test.test", "password":"12345678", "matNo":9999, "firstName":"test", "lastName":"test", "year":4}');
        $this->userController->signup($data);
    }

    protected function tearDown() :void {
        $data=json_decode('{"username":"test.test"}');
        $this->userController->deleteUser($data);
    }

    /**
     * @test
     * @dataProvider failedSignupProvider
     */
    public function testFailedSignup($str) {
        $data = json_decode($str);
        $this->assertSame( $this->userController->signup($data),'{"message":"Check your input data!"}');
    }


    public function failedSignupProvider():array {
        return [[''],
                ['{"username":"x.y"}'],
                ['{"username":"x.y", "password":"12345678"}'],
                ['{"username":"x.y", "password":"12345678"}'],
                ['{"username":"x.y", "password":"12345678", "matNo":44}'],
                ['{"username":"x.y", "password":"12345678", "matNo":44, "firstName":"x"}'],
                ['{"username":"x.y", "password":"12345678", "matNo":44, "firstName":"x", "lastName":"y"}'],
                ['{"username":"x.y", "password":"12345678", "matNo":44, "firstName":"x", "lastName":"y", "year":6}'],
                ['{"username":"x.y", "password":"12345678", "matNo":44, "firstName":"x", "lastName":"y", "year":6}'],
                ['{"username":"x.y", "password":"12345678", "matNo":44, "firstName":"x", "lastName":"y", "year":-1}'],
                ['{"username":"x.y", "password":"12345678", "matNo":-51, "firstName":"x", "lastName":"y", "year":-1}'],
                ['{"username":"x.y", "password":"12345678", "matNo":"invalid", "firstName":"x", "lastName":"y", "year":-1}'],
                ['{"username":"x.y", "password":"12345678", "matNo":-51, "firstName":"x", "lastName":"y", "year":"invalid"}'],
               ];
    }

    /**
     * @test
     * @dataProvider signupExistignUserProvider
     */
    public function testSignupExistignUser($str) {
        $data = json_decode($str);
        $this->assertSame( $this->userController->signup($data),'{"message":"User already exists"}');
    }

    public function signupExistignUserProvider():array {
        return [
                ['{"username":"test.test", "password":"12345678", "matNo":9999, "firstName":"test", "lastName":"test", "year":4}'],
                ['{"username":"test.test2", "password":"12345678", "matNo":9999, "firstName":"test", "lastName":"test", "year":4}'],
               ];
    }

    public function testSignupSuccess(){
        $data=json_decode('{"username":"test2", "password":"12345678", "matNo":99999, "firstName":"test", "lastName":"test", "year":4}');
        $output=$this->userController->signup($data);
        $this->userController->deleteUser(json_decode('{"username":"test2"}'));
        $this->assertSame($output, '{"message":"User Registered Successfully."}');
    }

    /************************************* */

    /**
     * @test
     * @dataProvider failedLoginProvider
     */
    public function testFailedLogin($str) {
        $data = json_decode($str);
        $this->assertSame( $this->userController->login($data),'{"message":"username & password can\'t be empty"}');
    }


    public function failedLoginProvider():array {
        return [[''],
                ['{"username":"x.y"}'],
                ['{"password":"12345678"}'],
               ];
    }
    
    public function testLoginSuccess() {
        $data = json_decode('{"username":"test.test", "password":"12345678"}');
        $this->assertSame( $this->userController->login($data), '{"message":"User logged in!","user_information":{"username":"test.test","matNo":9999,"firstName":"test","lastName":"test","role":3,"balance":0,"approved":false}}');
    }

    public function testLoginUnauthorized() {
        $data = json_decode('{"username":"test.test", "password":"123456789"}');
        $this->assertSame( $this->userController->login($data), '{"message":"Failed to log in! User not found"}');
    }

    public function testLoginNotfound() {
        $data = json_decode('{"username":"test.test.test.test.test.test", "password":"123456789"}');
        $this->assertSame( $this->userController->login($data), '{"message":"Failed to log in! User not found"}');
    }

    public function testLoginNotfoundWithExtraFields() {
        $data = json_decode('{"username":"test.test", "password":"123456789", "x":"y"}');
        $this->assertSame( $this->userController->login($data), '{"message":"Failed to log in! User not found"}');
    }

    /**************************************** */

    public function testApproveUserSuccess() {
        $data = json_decode('{"username":"test.test"}');
        $this->assertSame( $this->userController->approveUser($data), '{"message":"User approved successfully."}');
    }

    public function testApproveUserNoFound() {
        $data = json_decode('{"username":"test.test46756768678"}');
        $this->assertSame( $this->userController->approveUser($data), '{"message":"No such user."}');
    }

    /**
     * @test
     * @dataProvider approveUserDataEmptyProvider
     */
    public function testApproveUserDataEmpty($str) {
        $data = json_decode($str);
        $this->assertSame( $this->userController->approveUser($data), '{"message":"Data is empty!"}');
    }

    public function approveUserDataEmptyProvider():array {
        return [[''],
                ['{"username2":"x.y"}'],
               ];
    }

    /******************************* */

    public function testDeleteUserSuccess() {
        $data=json_decode('{"username":"test2", "password":"12345678", "matNo":99999, "firstName":"test", "lastName":"test", "year":4}');
        $this->userController->signup($data);
        $output=$this->userController->deleteUser(json_decode('{"username":"test2"}'));
        $this->assertSame($output, '{"message":"User was removed successfully."}');
    }

    public function testDeleteUserNotFound() {
        $output=$this->userController->deleteUser(json_decode('{"username":"test22"}'));
        $this->assertSame($output, '{"message":"No such user."}');
    }

    /**
     * @test
     * @dataProvider deleteUserDataEmptyProvider
     */
    public function testDeleteUserDataEmpty($str) {
        $data = json_decode($str);
        $this->assertSame( $this->userController->deleteUser($data), '{"message":"Data is empty!"}');
    }

    public function deleteUserDataEmptyProvider():array {
        return [[''],
                ['{"username2":"x.y"}'],
               ];
    }

    /******************************************/

    /**
     * @test
     * @dataProvider updateBalanceDataEmptyProvider
     */
    public function testUpdateBalanceDataEmpty($str) {
        $data = json_decode($str);
        $this->assertSame( $this->userController->updateBalance($data), '{"message":"Check your input data"}');
    }

    public function updateBalanceDataEmptyProvider():array {
        return [[''],
                ['{"matNo":2345}'],
                ['{"matNo":"2345"}'],
                ['{"amount":1000}'],
                ['{"amount":"1000"}'],
                ['{"matNo":2345, "amount":-100}'],
                ['{"matNo":-2345, "amount":100}'],
                ['{"matNo":-2345, "amount":"1000"}'],
                ['{"matNo":"2345", "amount":1000}'],
                ['{"matNo":2345, "amount":"1000"}'],
               ];
    }

    public function testUpdateBalanceNotFound() {
        $data = $data=json_decode('{"matNo":999999999999, "amount":1000}');
        $this->assertSame( $this->userController->updateBalance($data), '{"message":"No such user."}');
    }

    public function testUpdateBalanceSuccess() {
        $data = $data=json_decode('{"matNo":9999, "amount":2000}');
        $this->assertSame( $this->userController->updateBalance($data), '{"message":"Balance updated successfully"}');
    }

    /*********************************** */

    /**
     * @test
     * @dataProvider getUnapprovedUsersDataEmptyProvider
     */
    public function testGetUnapprovedUsersDataEmpty($str) {
        $data = json_decode($str);
        $this->assertSame( $this->userController->getUnapprovedUsers($data), '{"message":"Check your input data"}');
    }

    public function getUnapprovedUsersDataEmptyProvider():array {
        return [[''],
                ['{"year":"2345"}'],
                ['{"year":-5}'],
                ['{"year":"10"}'],
               ];
    }

    public function testGetUnapprovedUsersSuccess() {
        $data = $data=json_decode('{"year":4}');
        $this->assertStringContainsString('{"username":"test.test","matNo":"9999","firstName":"test","lastName":"test","year":"4"}', $this->userController->getUnapprovedUsers($data));
    }

    public function testDontGetapprovedUsersSuccess() {
        //aprove test.test user
        $data = json_decode('{"username":"test.test"}');
        $this->userController->approveUser($data);
        //add another account in the same year to return
        $data=json_decode('{"username":"x.y.z", "password":"12345678", "matNo":22222, "firstName":"x", "lastName":"y", "year":4}');
        $this->userController->signup($data);
        //fetch Unaproved users
        $data=json_decode('{"year":4}');
        $output = $this->userController->getUnapprovedUsers($data);
        //delete user x.y.z
        $data=json_decode('{"username":"x.y.z"}');
        $this->userController->deleteUser($data);
        //check
        $this->assertStringNotContainsString('{"username":"test.test","matNo":"9999","firstName":"test","lastName":"test","year":"4"}', $output);
    }

}