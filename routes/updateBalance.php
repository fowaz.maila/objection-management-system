<?php
include_once "../controller/userController.php";
include_once "../config/headers.php";

header("Content-Type: application/json");
header("Access-Control-Allow-Methods: POST");

$data = json_decode(file_get_contents("php://input"));

$userController = new userController();

echo $userController->updateBalance($data);