<?php

include_once "../controller/objectionController.php";

header("Content-Type: application/json");
header("Access-Control-Allow-Methods: POST");

$data = json_decode(file_get_contents("php://input"));

$objectionController = new objectionController();

echo $objectionController->getUserObjections($data);