<?php

include_once dirname(dirname(__FILE__))."/config/constants.php";

class User
{
    private string $username;
    private string $password;
    private int $matNo;
    private string $firstName;
    private string $lastName;
    private int $role;
    private int $balance;
    private bool $approved;

    private string $table = "accounts";
    private static PDO $connect;

    function __construct(PDO $conn)
    {
        self::$connect=$conn;
    }

    function registerUser($data)
    {


        [$flag, $exception]=$this->isUserExist($data->username);

        if (!$flag)
        {
            $q = "INSERT INTO $this->table (username, password, matNo, firstName, lastName, year) VALUES (?,?,?,?,?,?)";
            try {
             $stmt = self::$connect->prepare($q);
                $password = password_hash($data->password, PASSWORD_BCRYPT);
                $stmt->bindParam(1, $data->username);
                $stmt->bindParam(2,$password);
                $stmt->bindParam(3, $data->matNo);
                $stmt->bindParam(4, $data->firstName);
                $stmt->bindParam(5, $data->lastName);
                $stmt->bindParam(6, $data->year);

                //execute
                $stmt->execute();

                return [CREATED, null];

            } catch (PDOException $exception) {

                return [CONFLICT, $exception];
            }
        }
        else if (!empty($exception))
        {
            
            return [BAD_REQUEST, $exception];
        }
        else
        {
            return [CONFLICT, null];
        }
        
    }

    private function isUserExist ($username)
    {
        $q = "SELECT username, password, matNo, firstName, lastName, role, balance, approved  FROM $this->table WHERE username=?";

        try {
            $stmt = self::$connect->prepare($q);
            $stmt->bindParam(1, $username);
            $stmt->execute();

            $rowCount = $stmt->rowCount();

            if ($rowCount > 0)
            {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $this->username = $row['username'];
                $this->password = $row['password'];
                $this->matNo = $row['matNo'];
                $this->firstName = $row['firstName'];
                $this->lastName = $row['lastName'];
                $this->role = $row['role'];
                $this->balance = $row['balance'];
                $this->approved = $row['approved'];

                return [true, null];
            }

        } catch (PDOException $exception) {
            return [false, $exception];
        }

        return [false, null];
    }

    function login ($username, $password)
    {
        [$flag, $exception]=$this->isUserExist($username);

        if ($flag)
        {
            if (password_verify($password, $this->password))
            {
                return [OK, ["username"=>$this->username, "matNo"=>$this->matNo,
                    "firstName"=>$this->firstName, "lastName"=>$this->lastName,
                    "role"=>$this->role, "balance"=>$this->balance, "approved"=>$this->approved]];
            }
            else
            {
                return [NOT_FOUND, null];
            }
        }
        else if (!empty($exception))
        {
            return [BAD_REQUEST, $exception];
        }
        else
        {
            return [NOT_FOUND, null];
        }
    }

    function approveUser ($username) {

        [$flag, $exception]=$this->isUserExist($username);

        if ($flag)
        {
            $q= "UPDATE $this->table SET approved=1 WHERE username=?";

            try {
                $stmt = self::$connect->prepare($q);
                $stmt->bindParam(1, $username);
                $stmt->execute();
                return [OK, null];
            } catch (PDOException $exception) {
                return [BAD_REQUEST, $exception];
            }
        }
        else if (!empty($exception))
        {
            return [BAD_REQUEST, $exception];
        }
        else
        {
            return [NOT_FOUND, null];
        }

    }

    function getUnapprovedUsers ($data) {
        $q = "SELECT username, matNo, firstName, lastName, year FROM $this->table WHERE approved=0 and year=? order by matNo";

        try {

            $stmt = self::$connect->prepare($q);
            $stmt->bindParam(1, $data->year);
            $stmt->execute();
            $rowCount = $stmt->rowCount();

            if($rowCount>0)
            {
                $users_array = array();
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
                {
                    $user = array(
                        "username"=>$row['username'],
                        "matNo"=>$row['matNo'],
                        "firstName"=>$row['firstName'],
                        "lastName"=>$row['lastName'],
                        "year"=>$row['year']
                    );

                    array_push($users_array, $user);
                }

                return [OK, $users_array];
            } else {
                return [NOT_FOUND, null];
            }

        } catch (PDOException $exception) {
            return [BAD_REQUEST, $exception];
        }
    }

    private function isUserExistBymatNo ($matNo)
    {
        $q = "SELECT *  FROM $this->table WHERE matNo=?";

        try {
            $stmt = self::$connect->prepare($q);
            $stmt->bindParam(1, $matNo);
            $stmt->execute();

            $rowCount = $stmt->rowCount();

            if ($rowCount > 0)
            {

                return [true, null];
            }

        } catch (PDOException $exception) {
            return [false, $exception];
        }

        return [false, null];
    }


    function updateBalance ($data) {

        [$flag, $exception]=$this->isUserExistBymatNo($data->matNo);

        if ($flag)
        {
            $q="UPDATE $this->table SET balance=balance + (?) WHERE matNo=?";

            try {
                $stmt = self::$connect->prepare($q);
                $stmt->bindParam(1, $data->amount);
                $stmt->bindParam(2, $data->matNo);
                $stmt->execute();
                return [OK, null];
            } catch (PDOException $exception) {
                return [BAD_REQUEST, $exception];
            }
        }
        else if (!empty($exception))
        {
            return [BAD_REQUEST, $exception];
        }
        else
        {
            return [NOT_FOUND, null];
        }

    }

    function deleteUser($data) {

        [$flag, $exception]=$this->isUserExist($data->username);

        if ($flag)
        {
            $q="DELETE from $this->table WHERE username=?";
            try {
                $stmt = self::$connect->prepare($q);
                $stmt->bindParam(1, $data->username);
                $stmt->execute();
                return [OK, null];
            } catch (PDOException $exception) {
                return [BAD_REQUEST, $exception];
            }
        }
        else if (!empty($exception))
        {
            return [BAD_REQUEST, $exception];
        }
        else
        {
            return [NOT_FOUND, null];
        }

    }
}