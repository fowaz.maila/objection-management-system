<?php

include_once dirname(dirname(__FILE__))."/config/constants.php";

class Objections
{
    private string $table = "objections";
    private static PDO $connect;


    function __construct(PDO $conn)
    {
        self::$connect=$conn;
    }

    private function isObjectionExist ($matNo, $courseTitle)
    {
        $q = "SELECT * FROM $this->table WHERE matNo=? && courseTitle=?";

        try {
            $stmt = self::$connect->prepare($q);
            $stmt->bindParam(1, $matNo);
            $stmt->bindParam(2, $courseTitle);
            $stmt->execute();

            $rowCount = $stmt->rowCount();

            if ($rowCount > 0)
            {
                return [true, null];
            }

        } catch (PDOException $exception) {
            return [false, $exception];
        }

        return [false, null];
    }

    private function isObjectionExistById ($id)
    {
        $q = "SELECT * FROM $this->table WHERE id=?";

        try {
            $stmt = self::$connect->prepare($q);
            $stmt->bindParam(1, $id);
            $stmt->execute();

            $rowCount = $stmt->rowCount();

            if ($rowCount > 0)
            {
                return [true, null];
            }

        } catch (PDOException $exception) {
            return [false, $exception];
        }

        return [false, null];
    }

    private function isUserExistBymatNo ($matNo)
    {
        $q = "SELECT *  FROM accounts WHERE matNo=?";

        try {
            $stmt = self::$connect->prepare($q);
            $stmt->bindParam(1, $matNo);
            $stmt->execute();

            $rowCount = $stmt->rowCount();

            if ($rowCount > 0)
            {

                return [true, null];
            }

        } catch (PDOException $exception) {
            return [false, $exception];
        }

        return [false, null];
    }

    function submitObjection($data)
    {
        [$flag1, $exception1]=$this->isUserExistBymatNo($data->matNo);

        if($flag1) {
            [$flag, $exception]=$this->isObjectionExist($data->matNo, $data->courseTitle);

            if (!$flag)
            {
                $q = "INSERT INTO $this->table (matNo, firstName, lastName, year, courseTitle, teacher, oldMark)
                VALUES (?,?,?,?,?,?,?)";
                $q2= "UPDATE accounts SET balance=balance -2000 WHERE matNo=?";
                try {
                    $stmt = self::$connect->prepare($q);
                    $stmt2 = self::$connect->prepare($q2);
                    $stmt->bindParam(1, $data->matNo);
                    $stmt->bindParam(2,$data->firstName);
                    $stmt->bindParam(3, $data->lastName);
                    $stmt->bindParam(4, $data->year);
                    $stmt->bindParam(5, $data->courseTitle);
                    $stmt->bindParam(6, $data->teacher);
                    $stmt->bindParam(7, $data->oldMark);
                    $stmt2->bindParam(1,$data->matNo);

                    //execute
                    try {
                        self::$connect->beginTransaction();
                        $stmt->execute();
                        $stmt2->execute();
                        self::$connect->commit();
                    } catch (PDOException $exception) {
                        self::$connect->rollBack();
                        return [BAD_REQUEST, $exception];
                    }

                    return [CREATED, null];

                } catch (PDOException $exception) {

                    return [CONFLICT, $exception];
                }
            }
            else if (!empty($exception))
            {
                return [BAD_REQUEST, $exception];
            }
            else
            {
                return [CONFLICT, null];
            }
        } else {
            return [NOT_FOUND, null]; 
        }

    }

    function updateStatus ($data) {

        [$flag, $exception]=$this->isObjectionExistById($data->id);

        if ($flag)
        {
            $q="UPDATE $this->table SET status= ? WHERE id=?";

            try {
                $stmt = self::$connect->prepare($q);
                $stmt->bindParam(1, $data->newStatus);
                $stmt->bindParam(2, $data->id);
                $stmt->execute();
                return [OK, null];
            } catch (PDOException $exception) {
                return [BAD_REQUEST, $exception];
            }
        }
        else if (!empty($exception))
        {
            return [BAD_REQUEST, $exception];
        }
        else
        {
            return [NOT_FOUND, null];
        }
  
    }

    function enterResult ($data) {

        [$flag, $exception]=$this->isObjectionExistById($data->id);

        if ($flag)
        {
            $q="UPDATE $this->table SET status = ?, newMark = ? WHERE id=?";

            try {

                $stmt = self::$connect->prepare($q);
                $stmt->bindParam(1, $data->newStatus);
                $stmt->bindParam(2, $data->newMark);
                $stmt->bindParam(3, $data->id);
                $stmt->execute();
                return [OK, null];
            } catch (PDOException $exception) {
                return [BAD_REQUEST, $exception];
            }
        }
        else if (!empty($exception))
        {
            return [BAD_REQUEST, $exception];
        }
        else
        {
            return [NOT_FOUND, null];
        }
    }

    function getUserObjections ($data) {
        $q = "SELECT id, matNo, firstName, lastName, year, courseTitle, teacher, oldMark, status, newMark FROM $this->table WHERE matNo=? ORDER BY year, courseTitle";

        try {

            $stmt = self::$connect->prepare($q);
            $stmt->bindParam(1, $data->matNo);
            $stmt->execute();
            $rowCount = $stmt->rowCount();

            if($rowCount>0)
            {
                $obligations_array = array();
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
                {
                    $obligation = array(
                        "id"=>$row['id'],
                        "matNo"=>$row['matNo'],
                        "firstName"=>$row['firstName'],
                        "lastName"=>$row['lastName'],
                        "year"=>$row['year'],
                        "courseTitle"=>$row['courseTitle'],
                        "teacher"=>$row['teacher'],
                        "oldMark"=>$row['oldMark'],
                        "status"=>$row['status'],
                        "newMark"=>$row['newMark']
                    );

                    array_push($obligations_array, $obligation);
                }

                return [OK, $obligations_array];
            } else {
                return [NOT_FOUND, null];
            }

        } catch (PDOException $exception) {
            return [BAD_REQUEST, $exception];
        }
    }

    function getObjectionsByStatusAndCourseTitle ($data) {
        $q = "SELECT id, matNo, firstName, lastName, year, courseTitle, teacher, oldMark, status, newMark FROM $this->table WHERE status=? && courseTitle=? order by matNo";

        try {

            $stmt = self::$connect->prepare($q);
            $stmt->bindParam(1, $data->status);
            $stmt->bindParam(2, $data->courseTitle);
            $stmt->execute();
            $rowCount = $stmt->rowCount();

            if($rowCount>0)
            {
                $obligations_array = array();
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
                {
                    $obligation = array(
                        "id"=>$row['id'],
                        "matNo"=>$row['matNo'],
                        "firstName"=>$row['firstName'],
                        "lastName"=>$row['lastName'],
                        "year"=>$row['year'],
                        "courseTitle"=>$row['courseTitle'],
                        "teacher"=>$row['teacher'],
                        "oldMark"=>$row['oldMark'],
                        "status"=>$row['status'],
                        "newMark"=>$row['newMark']
                    );

                    array_push($obligations_array, $obligation);
                }

                return [OK, $obligations_array];
            } else {
                return [NOT_FOUND, null];
            }

        } catch (PDOException $exception) {
            return [BAD_REQUEST, $exception];
        }
    }

    function deleteObjection($data) {

        [$flag, $exception]=$this->isObjectionExist($data->matNo, $data->courseTitle);

        if ($flag)
        {
            $q="DELETE from $this->table WHERE matNo=? && courseTitle=?";
            try {
                $stmt = self::$connect->prepare($q);
                $stmt->bindParam(1, $data->matNo);
                $stmt->bindParam(2, $data->courseTitle);
                $stmt->execute();
                return [OK, null];
            } catch (PDOException $exception) {
                return [BAD_REQUEST, $exception];
            }
        }
        else if (!empty($exception))
        {
            return [BAD_REQUEST, $exception];
        }
        else
        {
            return [NOT_FOUND, null];
        }

    }

}