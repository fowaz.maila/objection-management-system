<?php

class Courses {

    private string $table = "courses";
    private static PDO $connect;

    function __construct(PDO $conn)
    {
        self::$connect=$conn;
    }

    function CoursesByYear ($data) {
        if($data->year>5 || $data->year<1) {
            return [BAD_REQUEST, null];
        } else {

            try {
                $q="SELECT courseTitle from $this->table WHERE year=? order by courseTitle";
                $stmt = self::$connect->prepare($q);
                $stmt->bindParam(1, $data->year);
                $stmt->execute();

                $rowCount = $stmt->rowCount();

                if($rowCount>0)
                {
                    $courses_array = array();
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
                    {
                        array_push($courses_array, $row['courseTitle']);
                    }

                    return [OK, $courses_array];
                } else {
                    return [NOT_FOUND, null];
                }
            } catch (PDOException $exception) {
                return [BAD_REQUEST, $exception];
            }
        }
    }

}