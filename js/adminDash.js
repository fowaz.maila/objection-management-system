//get current user info
let currentUser = JSON.parse(localStorage.getItem("currentUser"));

// Selecting Elements
const logOut = document.querySelector(".log-out");
const requestBox = document.querySelector(".requests-window");
let btnAccept = document.querySelectorAll(".btn-accept");
let btnReject = document.querySelectorAll(".btn-reject");
let labelUser = document.querySelector("#navbarDarkDropdownMenuLink");
const modalBody = document.querySelector(".modal-body");
const modalTitle = document.querySelector(".modal-title");
let btnConfirm = document.querySelector(".btn-confirm");
const yearSelector = document.querySelector(".year-sel");

//set the user label
labelUser.textContent = `${currentUser.firstName} ${currentUser.lastName}`;

const displaySignupRequests = function (year) {
  //empty the container
  requestBox.innerHTML = "";

  //get the signup requests
  fetch("http://localhost/SemesterProject/routes/getUnapprovedUsers.php", {
    method: "post",
    body: JSON.stringify({
      year: year,
    }),
  })
    .then(function (res) {
      return Promise.all([res.status, res.json()]);
    })
    .then(function ([status, signupRequests]) {
      if (status === 200) {
        signupRequests.forEach(function (req) {
          const html = `<div class="container-fluid request removable">
          <div class="container-fluid d-flex align-items-center row">
            <div class="col-lg-4 text-center">
              <h5>Name: ${req.firstName} ${req.lastName}</h5>
            </div>
            <div class="col-lg-3 text-center">
              <h5>matNo: ${req.matNo}</h5>
            </div>
            <div class="col-lg-3 text-center">
              <h5>Year: ${req.year}</h5>
            </div>
            <div class="col-lg-2 text-center">
              <button id="${req.username}" class="btn btn-primary btn-accept" data-bs-toggle="modal"
              data-bs-target="#staticBackdrop">&#10003;</button>
              <button id="${req.username}" class="btn btn-danger btn-reject" data-bs-toggle="modal"
              data-bs-target="#staticBackdrop">&#x2715;</button>
            </div>
          </div>
        </div>`;
          requestBox.insertAdjacentHTML("beforeend", html);
        });

        accpetBtnListeners();
        rejectBtnListeners();
      } else {
        const html = `<div class="container-fluid objection removable">
                          <div class="container-fluid row row-cols-1">
                            <div class="col text-center">
                              <h5> There's No Signup Requests</h5>
                            </div>
                        </div>
                        </div>`;
        requestBox.insertAdjacentHTML("beforeend", html);
      }
    })
    .catch(function (error) {
      console.error(error);
    });
};

const rejectBtnListeners = function () {
  //select all the buttons
  btnReject = document.querySelectorAll(".btn-reject");
  //add event listeners to reject buttons
  btnReject.forEach((el) => {
    el.addEventListener("click", () => {
      modalTitle.textContent = "Confirmation";

      //get clicked student info to show it in the modal
      const name = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[1].textContent;
      const matNo = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[3].textContent;
      const year = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[5].textContent;
      modalBody.textContent = `Are You Sure you want to delete this user from the system ?
        ${name}, ${matNo}, ${year}`;

      //these 2 lines are important in order to delete all previous
      //event listeners associated with the confirm button
      btnConfirm.parentNode.replaceChild(
        btnConfirm.cloneNode(true),
        btnConfirm
      );
      btnConfirm = document.querySelector(".btn-confirm");

      btnConfirm.addEventListener("click", () => {
        fetch("http://localhost/SemesterProject/routes/deleteUser.php", {
          method: "post",
          body: JSON.stringify({
            username: el.id,
          }),
          headers: { "Content-Type": "application/json" },
        })
          .then(function (res) {
            //remove this request when we make sure it has been processed successfully
            if (res.status === 200) {
              requestBox.removeChild(
                el.parentElement.parentElement.parentElement
              );
              successAlert(
                `The following user ${name}, ${matNo}, ${year} was rejected successfully.`
              );
            } else {
              dangerAlert(`There was a problem rejecting this user.`);
            }

            //when box become empty display a message
            if (!requestBox.hasChildNodes()) {
              const html = `<div class="container-fluid objection removable">
                          <div class="container-fluid row row-cols-1">
                            <div class="col text-center">
                              <h5> There's No Signup Requests</h5>
                            </div>
                        </div>
                        </div>`;
              requestBox.insertAdjacentHTML("beforeend", html);
            }
          })
          .catch(function (error) {
            console.error(
              "Something Went Wrong with handling the promise...",
              error
            );
          });
      });
    });
  });
};

const accpetBtnListeners = function () {
  //select all accpet buttons
  btnAccept = document.querySelectorAll(".btn-accept");

  //add event listeners to accept buttons
  btnAccept.forEach((el) => {
    el.addEventListener("click", () => {
      modalTitle.textContent = "Confirmation";

      //get clicked student info to show it in the modal
      const name = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[1].textContent;
      const matNo = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[3].textContent;
      const year = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[5].textContent;
      modalBody.textContent = `Are You Sure you want to add this user to the system ?
        ${name}, ${matNo}, ${year}`;

      //these 2 lines are important in order to delete all previous
      //event listeners associated with the confirm button
      btnConfirm.parentNode.replaceChild(
        btnConfirm.cloneNode(true),
        btnConfirm
      );
      btnConfirm = document.querySelector(".btn-confirm");

      btnConfirm.addEventListener("click", () => {
        fetch("http://localhost/SemesterProject/routes/approveUser.php", {
          method: "post",
          body: JSON.stringify({
            username: el.id,
          }),
          headers: { "Content-Type": "application/json" },
        })
          .then(function (res) {
            //remove this request when we make sure it has been processed successfully
            //remove this request when we make sure it has been processed successfully
            if (res.status === 200) {
              requestBox.removeChild(
                el.parentElement.parentElement.parentElement
              );
              successAlert(
                `The following user ${name}, ${matNo}, ${year} was approved successfully.`
              );
            } else {
              dangerAlert(`There was a problem approving this user.`);
            }

            //when box become empty display a message
            if (!requestBox.hasChildNodes()) {
              const html = `<div class="container-fluid objection removable">
                              <div class="container-fluid row row-cols-1">
                                <div class="col text-center">
                                  <h5> There's No Signup Requests</h5>
                                </div>
                            </div>
                            </div>`;
              requestBox.insertAdjacentHTML("beforeend", html);
            }
          })
          .catch(function (error) {
            console.error(
              "Something Went Wrong with handling the promise..." + error
            );
          });
      });
    });
  });
};

// toggling alerts
const dangerAlert = function (msg) {
  document
    .querySelector(".alert-wait")
    .parentElement.classList.toggle("hidden");
  document.querySelector(".alert-wait").textContent = msg;

  setTimeout(() => {
    document
      .querySelector(".alert-wait")
      .parentElement.classList.toggle("hidden");
  }, 3000);
};

const successAlert = function (msg) {
  document.querySelector(".alert-ok").parentElement.classList.toggle("hidden");
  document.querySelector(".alert-ok").textContent = msg;
  setTimeout(() => {
    document
      .querySelector(".alert-ok")
      .parentElement.classList.toggle("hidden");
  }, 3000);
};

logOut.addEventListener("click", () => {
  currentUser = undefined;
  localStorage.setItem("currentUser", undefined);
  localStorage.clear();
});

// getting objections by year
yearSelector.addEventListener("change", () => {
  displaySignupRequests(+yearSelector.value);
});

//delete on login
requestBox.innerHTML = "";
