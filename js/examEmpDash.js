let currentUser = JSON.parse(localStorage.getItem("currentUser"));

// Selecting global Elements
const chargePane = document.querySelector(".tab-pane-charge");
const viewPane = document.querySelector(".tab-pane-view");
const chargeTab = document.querySelector(".tab-pill-charge");
const viewTab = document.querySelector(".tab-pill-view");
const labelUser = document.querySelector("#navbarDarkDropdownMenuLink");
const logOut = document.querySelector(".log-out");

// Selecting elements from the charge account tab
const matNo = document.querySelector(".charge-matno");
const Amount = document.querySelector(".charge-amount");
const submitButton = document.querySelector(".submit-button");
const modalBody = document.querySelector(".modal-body");
const cancelButton = document.querySelector(".btn-cancel");
let confirmButton = document.querySelector(".btn-confirm");
const modalTitle = document.querySelector(".modal-title");

// Selecting elements from the view my objections tab
const yearSelector = document.querySelector(".year-sel");
const subjectSelector = document.querySelector(".subject-sel");
const acceptButton = document.querySelector(".btn-accept");
const rejectButton = document.querySelector(".btn-reject");
const objectionsWindow = document.querySelector(".objections-window");

// Linking the two Dropdowns (Subjects With Year)
const changeSubjects = function (subjects) {
  subjectSelector.disabled = false;
  subjectSelector.innerHTML =
    "<option disabled selected hidden>المادة</option>";
  subjects.forEach((el) => {
    const html = `<option value="${el}">${el}</option>`;
    subjectSelector.insertAdjacentHTML("beforeend", html);
  });
};

yearSelector.addEventListener("change", (e) => {
  fetch("http://localhost/SemesterProject/routes/getCoursesByYear.php", {
    method: "post",
    body: JSON.stringify({
      year: Number(yearSelector.value),
    }),
    headers: { "Content-Type": "application/json" },
  })
    .then(function (res) {
      return Promise.all([res.status, res.json()]);
    })
    .then(function ([status, courses]) {
      if (status === 200) changeSubjects(courses);
      else dangerAlert("حصل خطأ ما أثناء الحصول على المواد");
    })
    .catch(function (error) {
      dangerAlert("حصل خطأ ما أثناء الحصول على المواد");
      console.error("Something Went Wrong with handling the promise...");
    });
});
// END

// disabling the submit button if the input fields are empty
matNo.addEventListener("input", () => {
  updateButtonStatus();
});

Amount.addEventListener("input", () => {
  updateButtonStatus();
});

const updateButtonStatus = function () {
  if (
    matNo.value != "" &&
    !isNaN(matNo.value) &&
    Amount.value != "" &&
    !isNaN(Amount.value)
  ) {
    submitButton.disabled = false;
  } else {
    submitButton.disabled = true;
  }
};

// init
subjectSelector.disabled = true;
updateButtonStatus();
labelUser.textContent = `${currentUser.firstName} ${currentUser.lastName}`;
objectionsWindow.innerHTML = "";

// Changing beteween charge account and View my objections Screens
chargeTab.addEventListener("click", () => {
  if (!viewPane.classList.contains("hidden")) {
    viewPane.classList.add("hidden");
  }
  if (chargePane.classList.contains("hidden")) {
    chargePane.classList.remove("hidden");
  }
});

viewTab.addEventListener("click", () => {
  if (!chargePane.classList.contains("hidden")) {
    chargePane.classList.add("hidden");
  }
  if (viewPane.classList.contains("hidden")) {
    viewPane.classList.remove("hidden");
  }
});

//End

// charging an account
submitButton.addEventListener("click", (e) => {
  e.preventDefault();
  modalBody.textContent = `ستقوم بشحن حساب الطالب الذي يملك الرقم الجامعي ${matNo.value} بالمبلغ ${Amount.value}ل.س، هل ترغب في المتابعة؟`;

  //these 2 lines are important in order to delete all previous
  //event listeners associated with the confirm button
  confirmButton.parentNode.replaceChild(
    confirmButton.cloneNode(true),
    confirmButton
  );
  confirmButton = document.querySelector(".btn-confirm");

  // confirm button for charging the account
  confirmButton.addEventListener("click", (e) => {
    //check if matNo is negative
    if (+matNo.value < 0) {
      dangerAlert("لا يمكن أن يكون الرقم الجامعي رقم سالب");
      return;
    }

    //check if matNo is a number
    if (isNaN(matNo.value) || isNaN(Amount.value)) {
      dangerAlert("الرقم الجامعي والمبلغ المشحون يجب أن يحتويا على أرقام فقط");
      return;
    }

    //check if matNo is a number
    if (+Amount.value <= 0) {
      dangerAlert("المبلع المشحون يجب أن يكون أكبر من الصفر");
      return;
    }

    //here goes the api call
    fetch("http://localhost/SemesterProject/routes/updateBalance.php", {
      method: "post",
      body: JSON.stringify({
        matNo: +matNo.value,
        amount: +Amount.value,
      }),
      headers: { "Content-Type": "application/json" },
    })
      .then(function (res) {
        return Promise.all([res.status, res.json()]);
      })
      .then(function ([status, result]) {
        if (status === 200) {
          successAlert("تم شحن المبلغ بنجاح");
        } else if (status === 404)
          dangerAlert("لا يوجد أي حساب مسجل بهذا الرقم الجامعي");
        else dangerAlert("حدث خطأ ما أثناء شحن المبلغ المطلوب");

        //clear fields
        matNo.value = "";
        Amount.value = "";
      })
      .catch(function (error) {
        dangerAlert("حصل خطأ ما أثناء شحن الحساب");
        console.error("Something Went Wrong with handling the promise...");
      });
  });
});

//get a specific course objection requests
const getCourseObjections = function (course) {
  //remove everything in objection window
  objectionsWindow.innerHTML = "";

  //get student objections
  fetch(
    "http://localhost/SemesterProject/routes/getObjectionsByStatusAndCourseTitle.php",
    {
      method: "post",
      body: JSON.stringify({
        status: "pending",
        courseTitle: course,
      }),
    }
  )
    .then(function (res) {
      return Promise.all([res.status, res.json()]);
    })
    .then(function ([status, stdObjections]) {
      if (status === 200) {
        stdObjections.forEach(function (obj) {
          const html = `<div class="container-fluid objection removable">
          <div
            class="container-fluid d-flex align-items-center text-center margin row row-cols-1 row-cols-lg-5"
            dir="rtl"
          >
            <div class="col">
              <h4 class="responsive-h4">الرقم الجامعي: ${obj.matNo}</h4>
            </div>
            <div class="col">
              <h4 class="responsive-h4">الاسم: ${obj.firstName} ${obj.lastName}</h4>
            </div>
            <div class="col">
              <h4 class="responsive-h4">العلامة المعلنة: ${obj.oldMark}</h4>
            </div>
            <div class="col">
              <h4 class="responsive-h4">اسم المدرّس: ${obj.teacher}</h4>
            </div>
            <div class="col">
              <button id="${obj.id}" class="btn btn-primary me-1 btn-accept" data-bs-toggle="modal"
              data-bs-target="#staticBackdrop">
                &#10003;
              </button>
              <button id="${obj.id}" class="btn btn-danger btn-reject" data-bs-toggle="modal"
              data-bs-target="#staticBackdrop">
                &#x2715;
              </button>
            </div>
          </div>
        </div>`;
          objectionsWindow.insertAdjacentHTML("beforeend", html);
        });
        rejectBtnListeners();
        accpetBtnListeners();
      } else {
        const html = `<h5 class="center">لا يوجد اعتراضات على هذا المقرر</h5>`;
        objectionsWindow.insertAdjacentHTML("beforeend", html);
      }
    })
    .catch(function (error) {
      console.error(error);
    });
};

subjectSelector.addEventListener("change", () => {
  getCourseObjections(subjectSelector.value);
});

// approving objections' requests
const accpetBtnListeners = function () {
  //select all the buttons
  btnAccept = document.querySelectorAll(".btn-accept");

  //add event listeners to Accept buttons
  btnAccept.forEach((el) => {
    el.addEventListener("click", () => {
      // getting the attributes of the objection
      const name = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[3].textContent;
      const matNo = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[1].textContent;
      const oldMark = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[5].textContent;
      const teacher = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[7].textContent;

      // edit the modal body
      modalBody.textContent = `هل ترغب حقاً في الموافقة على هذا الاعتراض الذي يملك المعلومات التالية
      ${name}, ${matNo}, ${oldMark}, ${teacher}`;

      //these 2 lines are important in order to delete all previous
      //event listeners associated with the confirm button
      confirmButton.parentNode.replaceChild(
        confirmButton.cloneNode(true),
        confirmButton
      );
      confirmButton = document.querySelector(".btn-confirm");

      confirmButton.addEventListener("click", (e) => {
        fetch(
          "http://localhost/SemesterProject/routes/updateObjectionStatus.php",
          {
            method: "post",
            body: JSON.stringify({
              id: +el.id,
              newStatus: "reviewing",
            }),
            headers: { "Content-Type": "application/json" },
          }
        )
          .then(function (res) {
            //remove this request when we make sure it has been processed successfully
            if (res.status === 200) {
              objectionsWindow.removeChild(
                el.parentElement.parentElement.parentElement
              );
              successAlert("تم قبول الاعتراض بنجاح");
            } else dangerAlert("حصل خطأ ما أثناء الموافقة على هذا الطلب");

            //when box become empty display a message
            if (!objectionsWindow.hasChildNodes()) {
              const html = `<h5 class="center">لا يوجد اعتراضات على هذا المقرر</h5>`;
              objectionsWindow.insertAdjacentHTML("beforeend", html);
            }
          })
          .catch(function (error) {
            console.error(
              "Something Went Wrong with handling the promise...",
              error
            );
          });
      });
    });
  });
};

//rejecting objections' requests
const rejectBtnListeners = function () {
  //select all the buttons
  btnReject = document.querySelectorAll(".btn-reject");
  //add event listeners to reject buttons
  btnReject.forEach((el) => {
    el.addEventListener("click", () => {
      // getting the attributes of the objection
      const name = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[3].textContent;
      const matNo = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[1].textContent;
      const oldMark = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[5].textContent;
      const teacher = document.getElementById(`${el.id}`).parentElement
        .parentElement.childNodes[7].textContent;

      // edit the modal body
      modalBody.textContent = `هل ترغب حقاً في رفض هذا الاعتراض الذي يملك المعلومات التالية
      ${name}, ${matNo}, ${oldMark}, ${teacher}`;

      //these 2 lines are important in order to delete all previous
      //event listeners associated with the confirm button
      confirmButton.parentNode.replaceChild(
        confirmButton.cloneNode(true),
        confirmButton
      );
      confirmButton = document.querySelector(".btn-confirm");

      confirmButton.addEventListener("click", (e) => {
        fetch(
          "http://localhost/SemesterProject/routes/updateObjectionStatus.php",
          {
            method: "post",
            body: JSON.stringify({
              id: +el.id,
              newStatus: "rejected",
            }),
            headers: { "Content-Type": "application/json" },
          }
        )
          .then(function (res) {
            //remove this request when we make sure it has been processed successfully
            if (res.status === 200) {
              objectionsWindow.removeChild(
                el.parentElement.parentElement.parentElement
              );
              successAlert("تم رفض هذا الاعتراض بنجاح");
            } else dangerAlert("حصل خطأ ما أثناء رفض هذا الطلب");

            //when box become empty display a message
            if (!objectionsWindow.hasChildNodes()) {
              const html = `<h5 class="center">لا يوجد اعتراضات على هذا المقرر</h5>`;
              objectionsWindow.insertAdjacentHTML("beforeend", html);
            }
          })
          .catch(function (error) {
            console.error(
              "Something Went Wrong with handling the promise...",
              error
            );
          });
      });
    });
  });
};

// toggling alerts (use the functions to toggle alerts on and off)
const dangerAlert = function (msg) {
  document
    .querySelector(".alert-wait")
    .parentElement.classList.toggle("hidden");
  document.querySelector(".alert-wait").textContent = msg;

  setTimeout(() => {
    document
      .querySelector(".alert-wait")
      .parentElement.classList.toggle("hidden");
  }, 3000);
};

const successAlert = function (msg) {
  document.querySelector(".alert-ok").parentElement.classList.toggle("hidden");
  document.querySelector(".alert-ok").textContent = msg;
  setTimeout(() => {
    document
      .querySelector(".alert-ok")
      .parentElement.classList.toggle("hidden");
  }, 3000);
};

logOut.addEventListener("click", () => {
  currentUser = undefined;
  localStorage.setItem("currentUser", undefined);
  localStorage.clear();
});
