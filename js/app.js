// Login Section

//Selecting Elements
const loginUsername = document.querySelector(".login-user");
const loginPassword = document.querySelector(".login-pass");
const loginButton = document.querySelector(".login-btn");
const exitButtonDanger = document.querySelector(".exit-btn");
const exitButtonSuccess = document.querySelector(".exit-btn-suc");

//Global Variable to hold the Information of the logged person
var currentUser;

//variable to hold the status of an API Call
let apiStatus;

loginButton.addEventListener("click", (e) => {
  e.preventDefault();
  // checking if the input is empty
  if (loginUsername.value.length == 0 || loginPassword.value.length == 0) {
    dangerAlert("Some of your field is empty.");
    return;
  }
  // here Goes the API Call and after you check for success or waiting you call the right function either successAlert or dangerAlert

  fetch("http://localhost/SemesterProject/routes/login.php", {
    method: "post",
    body: JSON.stringify({
      username: loginUsername.value,
      password: loginPassword.value,
    }),
    headers: { "Content-Type": "application/json" },
  })
    .then(function (res) {
      //get the response code
      apiStatus = res.status;

      //clear fields
      loginUsername.value = "";
      loginPassword.value = "";

      return res.json();
    })
    .then(function (js) {
      if (apiStatus === 200) {
        currentUser = js.user_information;
        localStorage.setItem("currentUser", JSON.stringify(currentUser));
        if (currentUser.role === 3)
          window.location.href =
            "http://localhost/SemesterProject/stdDash.html";
        else if (currentUser.role === 2)
          window.location.href =
            "http://localhost/SemesterProject/adminDash.html";
        else if (currentUser.role === 1)
          window.location.href =
            "http://localhost/SemesterProject/examEmpDash.html";
        else if (currentUser.role === 4)
          window.location.href =
            "http://localhost/SemesterProject/objectionCommitteeDash.html";
      } else if (apiStatus === 401) {
        //do something to show a message the the password is incorrect
        dangerAlert(js.message);
      } else {
        //do something to show a message the the password is incorrect
        dangerAlert(js.message);
      }
    })
    .catch(function (error) {
      console.error("Something Went Wrong with handling the promise...");
    });

  // making the button disabled for 2 seconds so the user cannot make more than 1 api call
  loginButton.setAttribute("disabled", "");
  setTimeout(() => {
    loginButton.removeAttribute("disabled");
  }, 2000);
});

// Sign Up Section

// Selecting Elements
const signupFirstName = document.querySelector(".signup-fname");
const signupLastName = document.querySelector(".signup-lname");
const signupUsername = document.querySelector(".signup-username");
const signupMatNo = document.querySelector(".signup-id");
const signupPassword = document.querySelector(".signup-pass");
const signupButton = document.querySelector(".signup-btn");
const signupYear = document.querySelector(".signup-year");

signupButton.addEventListener("click", (e) => {
  e.preventDefault();
  // checking if the input is empty
  if (
    signupFirstName.value.length == 0 ||
    signupLastName.value.length == 0 ||
    signupUsername.value.length == 0 ||
    signupMatNo.value.length == 0 ||
    signupPassword.value.length < 8
  ) {
    dangerAlert(
      "Some of your fields are empty, or your password isn't long enough."
    );
    return;
  }

  //check if matNo is Number
  if (isNaN(signupMatNo.value)) {
    dangerAlert("Your matNo can't contain letters.");
    return;
  }

  //check if matNo is negative
  if (+signupMatNo.value < 0) {
    dangerAlert("Your matNo can't be negative.");
    return;
  }

  if (isNaN(signupYear.value)) {
    dangerAlert("Please select a year.");
    return;
  }

  // here goes the api call
  fetch("http://localhost/SemesterProject/routes/signup.php", {
    method: "post",
    body: JSON.stringify({
      username: signupUsername.value.toLowerCase(),
      password: signupPassword.value,
      matNo: +signupMatNo.value, //+ sign to convert the value to number
      firstName: signupFirstName.value,
      lastName: signupLastName.value,
      year: +signupYear.value,
    }),
    headers: { "Content-Type": "application/json" },
  })
    .then(function (res) {
      //get the response code
      apiStatus = res.status;
      return res.json();
    })
    .then(function (js) {
      if (apiStatus == 201) {
        successAlert(js.message);

        //clear fields
        signupFirstName.value = "";
        signupLastName.value = "";
        signupUsername.value = "";
        signupMatNo.value = "";
        signupPassword.value = "";
      } else dangerAlert(js.message);
    });

  signupButton.setAttribute("disabled", "");
  setTimeout(() => {
    signupButton.removeAttribute("disabled");
  }, 3000);
});

// Alert Buttons

const dangerAlert = function (msg) {
  document
    .querySelector(".alert-wait")
    .parentElement.classList.toggle("hidden");
  document.querySelector(".alert-wait").textContent = msg;

  setTimeout(() => {
    document
      .querySelector(".alert-wait")
      .parentElement.classList.toggle("hidden");
  }, 3000);
};

exitButtonDanger.addEventListener("click", () => {
  document
    .querySelector(".alert-wait")
    .parentElement.classList.toggle("hidden");
});

const successAlert = function (msg) {
  document.querySelector(".alert-ok").parentElement.classList.toggle("hidden");
  document.querySelector(".alert-ok").textContent = msg;
  setTimeout(() => {
    document
      .querySelector(".alert-ok")
      .parentElement.classList.toggle("hidden");
  }, 3000);
};

exitButtonSuccess.addEventListener("click", () => {
  document.querySelector(".alert-ok").parentElement.classList.toggle("hidden");
});
