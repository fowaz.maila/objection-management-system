//get current user info
let currentUser = JSON.parse(localStorage.getItem("currentUser"));

// Selecting Elements
const logOut = document.querySelector(".log-out");
const objectionsBox = document.querySelector(".objections-window");
let labelUser = document.querySelector("#navbarDarkDropdownMenuLink");
const modalBody = document.querySelector(".modal-body");
const modalTitle = document.querySelector(".modal-title");
let confirmButton = document.querySelector(".btn-confirm");
const yearSelector = document.querySelector(".year-sel");
const subjectSelector = document.querySelector(".subject-sel");

//set the user label
labelUser.textContent = `${currentUser.firstName} ${currentUser.lastName}`;

// Linking the two Dropdowns (Subjects With Year)
subjectSelector.disabled = true;
const changeSubjects = function (subjects) {
  subjectSelector.disabled = false;
  subjectSelector.innerHTML =
    "<option disabled selected hidden>المقرر</option>";
  subjects.forEach((el) => {
    const html = `<option value="${el}">${el}</option>`;
    subjectSelector.insertAdjacentHTML("beforeend", html);
  });
};

yearSelector.addEventListener("change", (e) => {
  fetch("http://localhost/SemesterProject/routes/getCoursesByYear.php", {
    method: "post",
    body: JSON.stringify({
      year: Number(yearSelector.value),
    }),
    headers: { "Content-Type": "application/json" },
  })
    .then(function (res) {
      return Promise.all([res.status, res.json()]);
    })
    .then(function ([status, courses]) {
      if (status === 200) changeSubjects(courses);
      else dangerAlert("لقد حصل خطأ أثناء جلب المقررات");
    })
    .catch(function (error) {
      dangerAlert("لقد حصل خطأ ما، الرجاء المحاولة لاحقاً");
      console.error("Something Went Wrong with handling the promise...");
    });
});

// showing the approved objections
const displayApprovedObjections = function () {
  //empty the container
  objectionsBox.innerHTML = "";

  //get the signup approved objections
  fetch(
    "http://localhost/SemesterProject/routes/getObjectionsByStatusAndCourseTitle.php",
    {
      method: "post",
      body: JSON.stringify({
        courseTitle: subjectSelector.value,
        status: "reviewing",
      }),
      headers: { "Content-Type": "application/json" },
    }
  )
    .then(function (res) {
      return Promise.all([res.status, res.json()]);
    })
    .then(function ([status, signupRequests]) {
      if (status === 200) {
        signupRequests.forEach(function (req) {
          const html = `<div class="container-fluid objection removable">
          <div
            class="container-fluid text-center margin row row-cols-1 row-cols-lg-3"
          >
            <div class="col">
              <p>الاسم: ${req.firstName + " " + req.lastName}</p>
            </div>
            <div class="col">
              <p>الرقم الجامعي: ${req.matNo}</p>
            </div>
            <div class="col">
              <p>السنة: ${req.year}</p>
            </div>
            <div class="col">
              <p>العلامة المعلنة: ${req.oldMark}</p>
            </div>
            <div class="col">
              <p>المدرس: ${req.teacher}</p>
            </div>
            <div class="col">
              <p>- :العلامة الجديدة</p>
            </div>
            <div class="col text-center">
              <select id="${req.id}" class="form-select result-sel" required>
                <option disabled selected hidden>النتيجة</option>
                <option value="changed">changed</option>
                <option value="unchanged">unchanged</option>
              </select>
            </div>
            <div class="col text-center">
              <input
                id="${req.id}"
                class="form-control new-mark"
                type="text"
                placeholder="العلامة الجديدة"
              />
            </div>
            <div class="col">
                    <button
                      id="${req.id}"
                      class="btn btn-primary me-1 btn-accept"
                      data-bs-toggle="modal"
                      data-bs-target="#staticBackdrop"
                    >
                      &#10003;
                    </button>
                  </div>
          </div>
        </div>`;
          objectionsBox.insertAdjacentHTML("beforeend", html);
        });
        resultSelectors();
        accpetBtnListeners();
      } else {
        const html = `<div class="col text-center">
                              <h5> لا يوجد اعتراضات على هذا المقرر</h5>
                            </div>`;
        objectionsBox.insertAdjacentHTML("beforeend", html);
      }
    })
    .catch(function (error) {
      console.error(error);
    });
};

const resultSelectors = function () {
  const resultSelector = document.querySelectorAll(".result-sel");
  const newMarks = document.querySelectorAll(".new-mark");
  const btnAccept = document.querySelectorAll(".btn-accept");
  newMarks.forEach((el) => {
    el.disabled = true;
  });
  btnAccept.forEach((el) => {
    el.disabled = true;
  });
  resultSelector.forEach((el) => {
    el.addEventListener("change", (e) => {
      if (el.value == "changed") {
        //select newMark field & enable it
        let inputField = el.parentNode.parentNode.childNodes[15].childNodes[1];
        inputField.disabled = false;
        //disable send button
        el.parentNode.parentNode.childNodes[17].childNodes[1].disabled = true;

        //important to remove any old event listeners???? is it necessary?
        // it's not like confirm button where the button was shred between different events
        inputField.parentNode.replaceChild(
          inputField.cloneNode(true),
          inputField
        );
        inputField = el.parentNode.parentNode.childNodes[15].childNodes[1];

        inputField.addEventListener("input", () => {
          if (
            !isNaN(inputField.value) &&
            inputField.value != "" &&
            Number(inputField.value) <= 100 &&
            Number(inputField.value) >= 0
          ) {
            el.parentNode.parentNode.childNodes[17].childNodes[1].disabled = false;
          } else {
            el.parentNode.parentNode.childNodes[17].childNodes[1].disabled = true;
          }
        });
      } else {
        el.parentNode.parentNode.childNodes[15].childNodes[1].disabled = true;
        el.parentNode.parentNode.childNodes[15].childNodes[1].value = "";
        el.parentNode.parentNode.childNodes[17].childNodes[1].disabled = false;
      }
    });
  });
};

const accpetBtnListeners = function () {
  const btnAccept = document.querySelectorAll(".btn-accept");
  const newMarks = document.querySelectorAll(".new-mark");

  btnAccept.forEach((el) => {
    el.addEventListener("click", (e) => {
      const name = el.parentNode.parentNode.childNodes[1].textContent;
      const matNumber = el.parentNode.parentNode.childNodes[3].textContent;
      const oldMark = el.parentNode.parentNode.childNodes[7].textContent;
      const subj = subjectSelector.value;
      const resultSelector =
        el.parentNode.parentNode.childNodes[13].children[0].value;
      const newM = el.parentNode.parentNode.childNodes[15].childNodes[1].value; //check it

      const txt = `هل ترغب حقاً في تثبيت نتيجة الاعتراض التالي: 
      ${name}, ${matNumber}, ${subj}, ${oldMark} النتيجة: ${
        resultSelector === "changed"
          ? "متغيرة" + ` العلامة الجديدة: ${newM}`
          : "غير متغيرة"
      } `;

      modalBody.textContent = txt;

      //these 2 lines are important in order to delete all previous
      //event listeners associated with the confirm button
      confirmButton.parentNode.replaceChild(
        confirmButton.cloneNode(true),
        confirmButton
      );
      confirmButton = document.querySelector(".btn-confirm");

      confirmButton.addEventListener("click", (e) => {
        fetch(
          "http://localhost/SemesterProject/routes/enterObjectionResult.php",
          {
            method: "post",
            body: JSON.stringify({
              id: +el.id,
              newStatus: resultSelector,
              newMark: resultSelector == "changed" ? Number(newM) : null,
            }),
            headers: { "Content-Type": "application/json" },
          }
        )
          .then(function (res) {
            //remove this request when we make sure it has been processed successfully
            if (res.status === 200) {
              objectionsBox.removeChild(
                el.parentElement.parentElement.parentElement
              );
              successAlert("تمت معالجة الاعتراض بنجاح");
            } else dangerAlert("لقد حصل خطأ ما أثناء معالجة الاعتراض");

            //when box become empty display a message
            if (!objectionsBox.hasChildNodes()) {
              const html = `<h5 class="center">لا يوجد اعتراضات على هذا المقرر</h5>`;
              objectionsBox.insertAdjacentHTML("beforeend", html);
            }
          })
          .catch(function (error) {
            console.error(
              "Something Went Wrong with handling the promise...",
              error
            );
          });
      });
    });
  });
};

// toggling alerts
const dangerAlert = function (msg) {
  document
    .querySelector(".alert-wait")
    .parentElement.classList.toggle("hidden");
  document.querySelector(".alert-wait").textContent = msg;

  setTimeout(() => {
    document
      .querySelector(".alert-wait")
      .parentElement.classList.toggle("hidden");
  }, 3000);
};

const successAlert = function (msg) {
  document.querySelector(".alert-ok").parentElement.classList.toggle("hidden");
  document.querySelector(".alert-ok").textContent = msg;
  setTimeout(() => {
    document
      .querySelector(".alert-ok")
      .parentElement.classList.toggle("hidden");
  }, 3000);
};

logOut.addEventListener("click", () => {
  currentUser = undefined;
  localStorage.setItem("currentUser", undefined);
  localStorage.clear();
});

// getting objections by year
subjectSelector.addEventListener("change", () => {
  displayApprovedObjections(+yearSelector.value);
});

//delete on login
objectionsBox.innerHTML = "";
