//get current user info
let currentUser = JSON.parse(localStorage.getItem("currentUser"));

// Selecting Elements
const sendPane = document.querySelector(".tab-pane-send");
const viewPane = document.querySelector(".tab-pane-view");
const sendTab = document.querySelector(".tab-pill-send");
const viewTab = document.querySelector(".tab-pill-view");
const yearSelector = document.querySelector(".year-sel");
const subjectSelector = document.querySelector(".subject-sel");
const submitButton = document.querySelector(".submit-button");
const oldMark = document.querySelector(".send-old-mark");
const instructorName = document.querySelector(".send-instructor-name");
const balanceEl = document.querySelector(".balance");
const modalBody = document.querySelector(".modal-body");
const cancelButton = document.querySelector(".btn-cancel");
const confirmButton = document.querySelector(".btn-confirm");
const objectionBox = document.querySelector("#view");
const logOut = document.querySelector(".log-out");
const labelUser = document.querySelector("#navbarDarkDropdownMenuLink");

// initializing
subjectSelector.disabled = true;
balanceEl.textContent = `Balance: ${currentUser.balance} SYP`;

//set the user label
labelUser.textContent = `${currentUser.firstName} ${currentUser.lastName}`;

// Changing beteween Send Objection and View Objections Screens
sendTab.addEventListener("click", () => {
  //check if user account is approved before sending objection request
  if (currentUser.approved === false) {
    dangerAlert(
      "You can't send objection requests until your account is approved."
    );
    return;
  }

  if (!viewPane.classList.contains("hidden")) {
    viewPane.classList.add("hidden");
  }
  if (sendPane.classList.contains("hidden")) {
    sendPane.classList.remove("hidden");
  }
});

viewTab.addEventListener("click", () => {
  if (!sendPane.classList.contains("hidden")) {
    sendPane.classList.add("hidden");
  }
  if (viewPane.classList.contains("hidden")) {
    viewPane.classList.remove("hidden");
  }

  //get student objections
  getStudentObjections();
});

//End

//function to display the subjects
const changeSubjects = function (subjects) {
  subjectSelector.disabled = false;
  subjectSelector.innerHTML =
    "<option disabled selected hidden>Subject</option>";
  subjects.forEach((el) => {
    const html = `<option value="${el}">${el}</option>`;
    subjectSelector.insertAdjacentHTML("beforeend", html);
  });
};

// Linking the two Dropdowns (Subjects With Year)
yearSelector.addEventListener("change", (e) => {
  fetch("http://localhost/SemesterProject/routes/getCoursesByYear.php", {
    method: "post",
    body: JSON.stringify({
      year: Number(yearSelector.value),
    }),
    headers: { "Content-Type": "application/json" },
  })
    .then(function (res) {
      return Promise.all([res.status, res.json()]);
    })
    .then(function ([status, courses]) {
      if (status === 200) changeSubjects(courses);
      else dangerAlert("Something went wrong during fetching the courses.");
    })
    .catch(function (error) {
      dangerAlert("Something went wrong during fetching the courses.");
      console.error("Something Went Wrong with handling the promise...");
    });

  updateButtonStatus();
});
// END

// checking if the fields are empty to enable the submit button

subjectSelector.addEventListener("change", () => {
  updateButtonStatus();
});

oldMark.addEventListener("input", () => {
  updateButtonStatus();
});

instructorName.addEventListener("input", () => {
  updateButtonStatus();
});

const updateButtonStatus = function () {
  if (
    yearSelector.value != "Year" &&
    subjectSelector.value != "Subject" &&
    oldMark.value != "" &&
    !isNaN(oldMark.value) &&
    isNaN(instructorName.value) &&
    instructorName.value != ""
  ) {
    submitButton.disabled = false;
  } else {
    submitButton.disabled = true;
  }
};
updateButtonStatus();

// sending objection
submitButton.addEventListener("click", (e) => {
  e.preventDefault();

  // checking the balance
  if (currentUser.balance >= 2000) {
    if (confirmButton.classList.contains("hidden")) {
      confirmButton.classList.remove("hidden");
    }
    modalBody.textContent = `You will lose 2000 SYP if you wish to proceed.
    Your objection:
    Year: ${yearSelector.value}, Subject: ${subjectSelector.value}, Old Mark: ${oldMark.value}, Instructor's Name: ${instructorName.value}
    `;
  } else {
    modalBody.textContent = `You don't have enough balance to send an objection !
    You have ${currentUser.balance}$ and the objection costs 2000 SYP.
    `;
    if (!confirmButton.classList.contains("hidden")) {
      confirmButton.classList.add("hidden");
    }
  }
});

confirmButton.addEventListener("click", () => {
  balanceEl.textContent = `Balance: ${currentUser.balance} SYP`;

  //check if old mark is between 0..99
  if (+oldMark.value < 0 || +oldMark.value > 99) {
    dangerAlert("Your mark should be in range [0,99]!");
    return;
  }

  //check if old mark is a number
  if (isNaN(oldMark.value)) {
    dangerAlert("Mark can contain only digits!");
    return;
  }

  // here goes the api call
  fetch("http://localhost/SemesterProject/routes/submitObjection.php", {
    method: "post",
    body: JSON.stringify({
      matNo: +currentUser.matNo,
      firstName: currentUser.firstName,
      lastName: currentUser.lastName,
      year: Number(yearSelector.value),
      courseTitle: subjectSelector.value,
      teacher: instructorName.value,
      oldMark: +oldMark.value,
    }),
    headers: { "Content-Type": "application/json" },
  })
    .then(function (res) {
      return Promise.all([res.status, res.json()]);
    })
    .then(function ([status, result]) {
      if (status === 201) {
        successAlert(result.message);
        currentUser.balance = currentUser.balance - 2000;
        balanceEl.textContent = `Balance: ${currentUser.balance}$`;
      } else dangerAlert(result.message);
    })
    .catch(function (error) {
      dangerAlert("Something went wrong during submitting the objection");
      console.error("Something Went Wrong with handling the promise...");
    });

  //
});

// toggling alerts
const dangerAlert = function (msg) {
  document
    .querySelector(".alert-wait")
    .parentElement.classList.toggle("hidden");
  document.querySelector(".alert-wait").textContent = msg;

  setTimeout(() => {
    document
      .querySelector(".alert-wait")
      .parentElement.classList.toggle("hidden");
  }, 3000);
};

const successAlert = function (msg) {
  document.querySelector(".alert-ok").parentElement.classList.toggle("hidden");
  document.querySelector(".alert-ok").textContent = msg;
  setTimeout(() => {
    document
      .querySelector(".alert-ok")
      .parentElement.classList.toggle("hidden");
  }, 3000);
};

//display student objections
const getStudentObjections = function () {
  //remove everything in objection box
  objectionBox.innerHTML = "";

  //get student objections
  fetch("http://localhost/SemesterProject/routes/getUserObjections.php", {
    method: "post",
    body: JSON.stringify({
      matNo: currentUser.matNo,
    }),
  })
    .then(function (res) {
      return Promise.all([res.status, res.json()]);
    })
    .then(function ([status, stdObjections]) {
      if (status === 200) {
        stdObjections.forEach(function (obj) {
          const html = `<div class="container-fluid objection removable">
          <div
            class="container-fluid text-center margin row row-cols-1 row-cols-lg-3"
          >
            <div class="col">
              <p>Subject: ${obj.courseTitle}</p>
            </div>
            <div class="col">
              <p>Status: ${obj.status}</p>
            </div>
            <div class="col">
              <p>Year: ${obj.year}</p>
            </div>
            <div class="col">
              <p>Old Mark: ${obj.oldMark}</p>
            </div>
            <div class="col">
                    <p>Instructor: ${obj.teacher}</p>
            </div>
            <div class="col">
              <p>New Mark: ${obj.newMark == null ? "-" : obj.newMark}</p>
            </div>
          </div>
        </div>`;
          objectionBox.insertAdjacentHTML("beforeend", html);
        });
      } else {
        const html = `<h5 class="center">There's No Objection Requests</h5>`;
        objectionBox.insertAdjacentHTML("beforeend", html);
      }
    })
    .catch(function (error) {
      console.error(error);
    });
};

//logout
logOut.addEventListener("click", () => {
  currentUser = undefined;
  localStorage.setItem("currentUser", undefined);
  localStorage.clear();
});

//display a message when user logs in
if (currentUser.approved === false)
  dangerAlert("Your account is not yet approved.");
else {
  successAlert("Your account is approved");
}

//disaplay student obections when login
getStudentObjections();
