<?php

// include_once "../models/user.php";
// include_once "../config/database.php";
// include_once "../config/constants.php";

include_once dirname(dirname(__FILE__))."/models/user.php";
include_once dirname(dirname(__FILE__))."/config/database.php";
include_once dirname(dirname(__FILE__))."/config/constants.php";

class userController
{
    private User $user;

    function __construct()
    {
        $db = new database();
        $conn = $db->connect();
        $this->user = new User($conn);
    }

    function signup ($data) {
        if(!empty($data->username) && !empty($data->password) && !empty($data->matNo) && !empty($data->firstName) && !empty($data->lastName) && !empty($data->year) && (is_numeric($data->matNo))
        && (is_numeric($data->year)) && ($data->matNo>0) && ($data->year>0) && ($data->year<6))
        {
            [$code, $exception] = $this->user->registerUser($data);
            $output="";
            switch ($code)
            {
                case CREATED: $output=["message"=>"User Registered Successfully."]; break;
                case BAD_REQUEST: $output=["message"=>"Something went wrong! ".$exception->getMessage()]; break;
                case CONFLICT : $output=["message"=> "User already exists"]; break;
            }
        } else {
            $code = BAD_REQUEST;
            $output = ["message"=>"Check your input data!"];
        }

        http_response_code($code);
        return json_encode($output);
    }

    function login($data){
        if (!empty($data->username) && !empty($data->password))
        {
            [$code, $output]=$this->user->login($data->username, $data->password);
            switch ($code)
            {
                case OK: $json=["message" => "User logged in!", "user_information" =>$output];
                 break;

                case UNAUTHORIZED: $json = ["message" => "Failed to login! Wrong password"]; break;
                case BAD_REQUEST: $json = ["message"=>"Failed to log in! Something Went wrong ".$output->getmessage()]; break;
                case NOT_FOUND: $json = ["message" => "Failed to log in! User not found"]; break;
                default: $code = FORBIDDEN; $json = ["message"=>"Failed to log in! You are not allowed to log in!"]; break;

            }
        } else {
            $code = BAD_REQUEST;
            $json = ["message"=> "username & password can't be empty"];
        }

        http_response_code($code);
        return json_encode($json);
    }

    function approveUser($data) {
        if(!empty($data->username)) {
            [$code, $exception]=$this->user->approveUser($data->username);
            switch ($code)
            {
                case OK: $json = ["message"=> "User approved successfully."]; break;
                case BAD_REQUEST: $json = ["message"=>"Something went wrong in approving this user... ".$exception->getMessage()]; break;
                case NOT_FOUND: $json=["message"=> "No such user."]; break;
            }
        } else {
            $code = BAD_REQUEST;
            $json = ["message"=>"Data is empty!"];
        }
        http_response_code($code);
        return json_encode($json);
    }

    function getUnapprovedUsers($data)
    {

        if(!empty($data->year) && is_int($data->year) && ($data->year>0) && ($data->year<6))
        {
            [$code, $output] = $this->user->getUnapprovedUsers($data);
            switch ($code)
            {
                case OK: $json = $output; break;
                case NOT_FOUND: $json = ["message"=>"No data found!"]; break;
                case BAD_REQUEST: $json = ["message"=>"Failed to get unapproved users ".$output->getMessage()]; break;
                default: $code=FORBIDDEN; $json=["message"=>"You're not allowed to get to signup requests!"]; break;
            }
            
        } else {
            $code = BAD_REQUEST;
            $json= ["message"=>"Check your input data"];
        }

        http_response_code($code);
        return json_encode($json);
    }

    function updateBalance($data) {
        if(!empty($data->matNo) && !empty($data->amount) && (is_int($data->matNo)) && 
        (is_int($data->amount)) && ($data->matNo>0) && ($data->amount>0))
        {
            [$code, $exception] = $this->user->updateBalance($data);
            switch ($code)
            {
                case OK: $json=["message"=>"Balance updated successfully"]; break;
                case BAD_REQUEST: $json = ["message"=>"Couldn't Update Balance."]; break;
                case NOT_FOUND: $json=["message"=>"No such user."]; break;
                default: $json=["message"=>"Something went wrong"];
            }
        } else {
            $code = BAD_REQUEST;
            $json= ["message"=>"Check your input data"];
        }

        http_response_code($code);
        return json_encode($json);
    }

    function deleteUser($data) {
        if(!empty($data->username)) {
            [$code, $exception]=$this->user->deleteUser($data);
            switch ($code)
            {
                case OK: $json = ["message"=> "User was removed successfully."]; break;
                case BAD_REQUEST: $json = ["message"=>"Something went wrong in deleting this user... ".$exception->getMessage()]; break;
                case NOT_FOUND: $json=["message"=> "No such user."]; break;
            }
        } else {
            $code = BAD_REQUEST;
            $json = ["message"=>"Data is empty!"];
        }
        http_response_code($code);
        return json_encode($json);
    }

}