<?php
include_once "../models/Courses.php";
include_once "../config/database.php";
include_once "../config/constants.php";

class coursesController
{
    private Courses $courses;

    function __construct()
    {
        $db = new database();
        $conn = $db->connect();
        $this->courses = new Courses($conn);
    }

    function getCoursesByYear($data)
    {
        [$code, $output] = $this->courses->CoursesByYear($data);
        switch ($code)
        {
            case OK: $json = $output; break;
            case NOT_FOUND: $json = ["message"=>"No data found!"]; break;
            case BAD_REQUEST: if(!empty($output)) $json = ["message"=>"Something went wrong ".$output->getMessage()];
                                else $json = ["message"=>"Year is not valid"]; break;
        }

        http_response_code($code);
        return json_encode($json);
    }
}