<?php

// include_once "../models/Objections.php";
// include_once "../config/database.php";
// include_once "../config/constants.php";

include_once dirname(dirname(__FILE__))."/models/Objections.php";
include_once dirname(dirname(__FILE__))."/config/database.php";
include_once dirname(dirname(__FILE__))."/config/constants.php";

class objectionController
{

    private Objections $objection;

    function __construct()
    {
        $db = new database();
        $conn = $db->connect();
        $this->objection = new Objections($conn);
    }

    function submitObjection ($data)
    {
        if (!empty($data->matNo) && !empty($data->firstName) && !empty($data->lastName)
            && !empty($data->year) && !empty($data->courseTitle) && !empty($data->oldMark) && !empty($data->teacher) && is_int($data->matNo) && is_int($data->year) && is_int($data->oldMark)
            && ($data->matNo>0) && ($data->oldMark>=0) && ($data->year>0) && ($data->year<6)) {

            [$code, $exception] = $this->objection->submitObjection($data);
            $output = "";
            switch ($code) {
                case CREATED:
                    $output = ["message" => "Objection request submitted Successfully."];
                    break;
                case BAD_REQUEST:
                    $output = ["message" => "Something went wrong! " . $exception->getMessage()];
                    break;
                case CONFLICT :
                    $output = ["message" => "Objection request already exists"];
                    break;
                case NOT_FOUND :
                    $output = ["message" => "No such User"];
                    break;
            }
        } else {
            $code = BAD_REQUEST;
            $output = ["message" => "Check your input data!"];
        }

        http_response_code($code);
        return json_encode($output);
    }

    function updateStatus($data) {
        if(!empty($data->id) && !empty($data->newStatus) && is_int($data->id))
        {
            [$code, $exception] = $this->objection->updateStatus($data);
            switch ($code)
            {
                case OK: $json=["message"=>"Status updated successfully"]; break;
                case BAD_REQUEST: $json = ["message"=>"Couldn't Update Status."]; break;
                case NOT_FOUND: $json = ["message"=>"No such objection."]; break;
                default: $json=["message"=>"Something went wrong"];
            }
        } else {
            $code = BAD_REQUEST;
            $json= ["message"=>"check your input data"];
        }

        http_response_code($code);
        return json_encode($json);
    }

    function enterResult($data) {
        if(!empty($data->id) && !empty($data->newStatus) && is_int($data->id))
        {
            [$code, $exception] = $this->objection->enterResult($data);
            switch ($code)
            {
                case OK: $json=["message"=>"Result updated successfully"]; break;
                case BAD_REQUEST: $json = ["message"=>"Couldn't Update Result."]; break;
                case NOT_FOUND: $json = ["message"=>"No such objection."]; break;
                default: $json=["message"=>"Something went wrong"];
            }
        } else {
            $code = BAD_REQUEST;
            $json= ["message"=>"check your input data"];
        }

        http_response_code($code);
        return json_encode($json);
    }

    function getUserObjections($data)
    {
        if(!empty($data->matNo) && is_numeric($data->matNo)){
            [$code, $output] = $this->objection->getUserObjections($data);
        switch ($code)
        {
            case OK: $json = $output; break;
            case NOT_FOUND: $json = ["message"=>"You don't have any objections."]; break;
            case BAD_REQUEST: $json = ["message"=>"Failed to get your objections ".$output->getMessage()]; break;
            default: $code=FORBIDDEN; $json=["message"=>"You're not allowed to get your objections!"]; break;
        }
        }else {
            $code = BAD_REQUEST;
            $json= ["message"=>"check your input data"];
        }

        http_response_code($code);
        return json_encode($json);
    }

    function getObjectionsByStatusAndCourseTitle($data)
    {
        if(!empty($data->status)&&(!empty($data->courseTitle))) {
            [$code, $output] = $this->objection->getObjectionsByStatusAndCourseTitle($data);
        switch ($code)
        {
            case OK: $json = $output; break;
            case NOT_FOUND: $json = ["message"=>"There isn't any objections."]; break;
            case BAD_REQUEST: $json = ["message"=>"Failed to get objections ".$output->getMessage()]; break;
            default: $code=FORBIDDEN; $json=["message"=>"You're not allowed to get objections!"]; break;
        }
        }else {
            $code = BAD_REQUEST;
            $json= ["message"=>"check your input data"];
        }

        http_response_code($code);
        return json_encode($json);
    }

    function deleteObjection($data) {
        if(!empty($data->matNo) && !empty($data->courseTitle)) {
            [$code, $exception]=$this->objection->deleteObjection($data);
            switch ($code)
            {
                case OK: $json = ["message"=> "Objection was removed successfully."]; break;
                case BAD_REQUEST: $json = ["message"=>"Something went wrong in deleting this objection... ".$exception->getMessage()]; break;
                case NOT_FOUND: $json=["message"=> "No such objection."]; break;
            }
        } else {
            $code = BAD_REQUEST;
            $json = ["message"=>"Check your input data"];
        }
        http_response_code($code);
        return json_encode($json);
    }
}