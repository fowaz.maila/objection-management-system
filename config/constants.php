<?php

const OK = 200;
const CREATED = 201;

const BAD_REQUEST = 400;
const NOT_FOUND = 404;
const FORBIDDEN = 403;
const UNAUTHORIZED = 401;
const CONFLICT = 409;
