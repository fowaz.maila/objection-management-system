<?php


class database
{
    function  __construct()
    {
        include_once "env.php";
    }

    static function connect () : ?PDO
    {
        $conn = null;

        try {
            $conn = new PDO('mysql:127.0.0.1='.DB_HOST.';port='.DB_PORT.';dbname='.DB_NAME,DB_USERNAME, DB_PASSWORD);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) {
            echo 'Error! '.$exception->getMessage();
        }

        return $conn;
    }

}